package org.but.feec.javafx.exceptions;

public class ResourcesNotFoundExc extends RuntimeException
{
    public ResourcesNotFoundExc() {
    }

    public ResourcesNotFoundExc(String message) {
        super(message);
    }

    public ResourcesNotFoundExc(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourcesNotFoundExc(Throwable cause) {
        super(cause);
    }

    public ResourcesNotFoundExc(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
