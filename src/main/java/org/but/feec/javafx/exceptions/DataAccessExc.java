package org.but.feec.javafx.exceptions;

public class DataAccessExc extends RuntimeException
{

    public DataAccessExc() {
    }

    public DataAccessExc(String message) {
        super(message);
    }

    public DataAccessExc(String message, Throwable cause) {
        super(message, cause);
    }

    public DataAccessExc(Throwable cause) {
        super(cause);
    }

    public DataAccessExc(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
